import atexit
import binascii
import math
import os
import random
import sqlite3
import time
from pathlib import Path

from apscheduler.schedulers.background import BackgroundScheduler
from flask import (Flask, flash, g, jsonify, redirect, render_template,
                   request, send_file, send_from_directory)
from flask_simplelogin import SimpleLogin, login_required
from werkzeug.exceptions import InternalServerError

# load configuration
DB_PATH = os.environ.get('DB_PATH', 'database.db')
EXPIRE_DURATION_S = 24 * 60 * 60
CLEANUP_INTERVAL_S = 60 * 60

# limit the token length to the maximum possible one in base 10
MAX_TOKEN_STR_LEN = math.ceil(math.log(2**32 * 1000) / math.log(10))

# initialize flask
app = Flask(__name__)

# handler to get the singleton database object
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = sqlite3.connect(DB_PATH)
        g._database = db

    db.execute('CREATE TABLE IF NOT EXISTS tokens(expire_time_ms int, value varchar(32));')

    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db:
        db.close()


@app.route('/token')
def get_token():
    # create the token as the expire time in ms
    expire_time_s = time.time() + EXPIRE_DURATION_S
    expire_time_ms = int(expire_time_s * 1000)
    value = binascii.b2a_hex(os.urandom(16)).decode()

    # insert the token in the database
    connection = get_db()
    connection.cursor().execute('INSERT INTO tokens(expire_time_ms, value) VALUES(?, ?)', (expire_time_ms, value))
    connection.commit()

    return jsonify({
        'token': expire_time_ms,
        'value': value
    })


@app.route('/validate')
def validate_token():
    # get cursor from connection
    connection = get_db()
    cursor = connection.cursor()

    # token contains the expire time as per JavaScript convention
    expire_time_str = request.args.get("token")
    if len(expire_time_str) <= MAX_TOKEN_STR_LEN:
        # get the corresponding value
        expire_time_ms = int(expire_time_str)
        cursor.execute('SELECT expire_time_ms, value FROM tokens WHERE expire_time_ms=?', (expire_time_ms, ))

        if content := cursor.fetchone():
            # convert the time to seconds, as per python convention
            expire_time_s = expire_time_ms / 1000

            if time.time() < expire_time_s:
                # delete it from the database
                cursor.execute('DELETE FROM tokens WHERE expire_time_ms=?', (expire_time_ms, ))
                connection.commit()

                # return the value
                _, value = content
                return value

    return 'Invalid request'


@app.route('/')
def entry_page():
    return render_template('index.html')


# initialize the background scheduler for cleanups
def cleanup():
    # get cursor from connection
    connection = sqlite3.connect(DB_PATH)
    cursor = connection.cursor()

    # get current time in ms
    current_time_s = time.time()
    current_time_ms = int(current_time_s * 1000)

    # delete expired tokens, to clean up the database
    connection.cursor().execute('DELETE FROM tokens WHERE expire_time_ms<?', (current_time_ms, ))
    connection.commit()


scheduler = BackgroundScheduler()
scheduler.add_job(func=cleanup, trigger="interval", seconds=CLEANUP_INTERVAL_S)
scheduler.start()

atexit.register(lambda: scheduler.shutdown())


# run flask on debug mode
if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5009)
