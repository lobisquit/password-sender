FROM python:slim

# create a user and assign it the ownership of the database
RUN useradd --system --create-home runner

# create a persistent folder to put the db in
RUN mkdir /app_data/ && chown -R runner /app_data/
VOLUME ["/app_data/"]

# run everything as normal user from now on
USER runner
WORKDIR /home/runner/app

# upgrade pip to the latest-est version and install requirements
ENV PATH="/home/runner/.local/bin:${PATH}"
RUN pip install --upgrade pip

# install application dependencies
COPY app/requirements.txt requirements.txt
RUN pip install --user --no-cache-dir -r requirements.txt

# load actual application code, as a last step to
# prevent re-installs after code changes
COPY ./app /home/runner/app

USER runner
ENV DB_PATH=/app_data/database.db
ENTRYPOINT ["/home/runner/app/run.sh"]
